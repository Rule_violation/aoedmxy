import Vue from "vue";
import vueSwiper from "vue-awesome-swiper";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import "swiper/dist/css/swiper.css";
import "./assets/css/border.css";
import "./assets/css/reset.css";
import "./iconfont/iconfont.css";
Vue.config.productionTip = false;

Vue.use(vueSwiper);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");