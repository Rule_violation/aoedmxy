import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import ('./views/home/Home.vue')
    },
    {
      path: "/region",
      name: "region",
      component: () => import ('./views/region/Region.vue')
    },
	{
	  path: "/detalied/:id",
	  name: "detalied",
	  component: () => import ('./views/detalied/Detalied.vue')
	}
  ]
});