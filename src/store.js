import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

let defaultClity = '北京'
if (localStorage.city) {
  defaultClity = localStorage.city
}
export default new Vuex.Store({
  state: {
    cieIyl: '北京'
  },
  mutations: {
    selectionE (state ,city) {
      state.cieIyl = city
      localStorage.cieIyl = city
    }
  },
  actions: {
      selection (ctx , city) {
        ctx.commit('selectionE' , city)
      }
  }
});
